import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    const meta = {
      title: 'බෞද්ධ පුස්තකාලය',
      description: 'බෞද්ධ පොත් පහසුවෙන් කියවන්න.',
      image:
        '/sri_lanka_1024px.png'
    }

    return (
      <Html lang="en">
        <Head>
          <meta name="robots" content="follow, index" />
          <meta name="description" content={meta.description} />
          {/* <meta property="og:site_name" content={meta.title} /> */}
          {/* <meta property="og:description" content={meta.description} /> */}
          <meta property="og:title" content={meta.title} />
          {/* <meta property="og:image" content={meta.image} /> */}
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@prasadmadhuran1" />
          <meta name="twitter:title" content={meta.title} />
          <meta name="twitter:description" content={meta.description} />
          <meta name="twitter:image" content={meta.image} />
          {/* For Telegram IV render  */}
          <meta property="al:android:app_name" content="Medium" />
          <meta property="article:published_time" content="" />
          <meta name="telegram:channel" content="@buddhist_library" />
          <meta property="og:site_name" content="බෞද්ධ පුස්තකාලය" />
          <meta property="og:description" content="බෞද්ධ පොත් පහසුවෙන් කියවන්න" />
          <meta property="og:image" content="https://2img.now.sh/බෞද්ධ පුස්තකාලය.png?theme=dark&amp;fontFamily=alakamanda&amp;fontSize=400px" />
          <meta name="author" content="SINHAGIRI VISUAL STUDIO™" />
          <link href="../styles/bootstrap.css" rel="stylesheet" />
          <link href="../styles/bootstrap.min.css" rel="stylesheet" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
